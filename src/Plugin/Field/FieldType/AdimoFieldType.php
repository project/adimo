<?php

namespace Drupal\adimo\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'Adimo' field type.
 *
 * @FieldType(
 *   id = "field_adimo",
 *   label = @Translation("Adimo"),
 *   module = "adimo",
 *   description = @Translation("Introduce the lightbox Adimo id to display a
 *   Adimo widget."), category = @Translation("General"),
 *   default_widget = "adimo_widget", default_formatter =
 *   "adimo_formatter"
 * )
 */
class AdimoFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type'     => 'text',
          'size'     => 'tiny',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Price Spider button'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();

    return $value === NULL || $value === '';
  }

}
