<?php

namespace Drupal\adimo\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'field_adimo' formatter.
 *
 * @FieldFormatter(
 *   id = "adimo_formatter",
 *   module = "adimo",
 *   label = @Translation("Adimo widget formatter"),
 *   field_types = {
 *     "field_adimo"
 *   }
 * )
 */
class AdimoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $html_id = 'adimo-' . rand(0, 999999);
    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = [
        '#theme'              => 'adimo',
        '#data_touchpoint_id' => $item->value,
        '#html_id'            => $html_id,
        '#attached'           => [
          'library' => [
            'adimo/adimo',
            'adimo/adimo-connector',
          ],
        ],
      ];
    }
    return $element;
  }

}
