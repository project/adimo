<?php

namespace Drupal\adimo\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'adimo_widget' widget.
 *
 * @FieldWidget(
 *   id = "adimo_widget",
 *   module = "adimo",
 *   label = @Translation("Adimo Integration Widget"),
 *   field_types = {
 *     "field_adimo"
 *   }
 * )
 */
class AdimoWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type'             => 'textfield',
      '#title'            => $this->t('Adimo'),
      '#default_value'    => $value,
      '#size'             => 50,
      '#maxlength'        => 255,
      '#required'         => $element['#required'],
      '#element_validate' => [
        [$this, 'validate'],
      ],
    ];

    return ['value' => $element];
  }

  /**
   * Validate the text field.
   */
  public function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) === 0) {
      $form_state->setValueForElement($element, '');

      return;

    }

  }

}
