/**
 * @file
 * Contains the definition of the behaviour adimoWidget.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * Attaches the JS test behavior to weight div.
   */
  Drupal.behaviors.jsBuyNowAdimo = {
    attach: function (context, settings) {
      Adimo.main.init('https://basketads.adimo.co', false);
    }
  };
})(jQuery, Drupal, drupalSettings);
